Hangouts permanent link generator
=================================

As a pre-requisite for all the following tasks, you must navigate to the project directory:

    cd ~/redirectGenerator


Create a new permanent link
---------------------------

Run the script `redirectGenerator.rb` with 2 params:

1)  the initial hangouts url

2) an identifier. It should not contain spaces or punctuation except '-'.


Example:

    ruby redirectGenerator.rb http://some/hangouts/link 2016-10-05-test-link


Update existing permanent link to a new destination
---------------------------------------------------

Same command as above, this time with new destination link. Take care to use the exact same identifier otherwise istead of updating the existing permanent link you will create a new one.

Example
    
    ruby redirectGenerator.rb http://new/hangouts/link 2016-10-05-test-link


View the permanent link log
---------------------------

Every permanent link generated and every new hangouts link it points to is saved into a log file. You may want to check the log if you dont remember the latest identifier used or if you want to track old hangouts links. You can preview the log with:

    cat log.txt


Installing aws CLI
------------------

Follow these [instructions](http://docs.aws.amazon.com/cli/latest/userguide/installing.html#choosing-an-installation-method).

Then configure with your credentials with:

    aws configure --profile profilename

Note: use '100mentors' as the profile name.