require 'time'

aws_profile_name = '100mentors'

fail("Usage: <redirect-url> [<session-id>]") if ARGV.empty?

redirect_url = ARGV[0]
session_id = ARGV[1] || Time.now.strftime("%Y-%m-%d_%H%M%S")


html = <<-eos
<html>
  <head>
    <script type="text/javascript">
      window.location = '#{redirect_url}';
    </script>
  </head>
  <body onLoad="setTimeout('delayer()', 0)">
    <h1>Taking you to your hangouts session!</h1>
    <p>
      If you are not redirected automatically, please go to
      <a href="#{redirect_url}">#{redirect_url}</a>
    </p>
  </body>
</html>
eos

filename = "#{session_id}.html"

File.open(filename, 'w') do |f|
  f.write(html)
end

`aws --profile #{aws_profile_name} s3 cp #{filename} s3://100mentors-redirector/#{filename} --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers`

public_url = 'https://s3-eu-west-1.amazonaws.com/100mentors-redirector'

puts public_url + "/#{filename}"

`rm #{filename}`


# Log the redirect entry

`aws --profile #{aws_profile_name} s3 cp s3://100mentors-redirector/log.txt log.txt`
File.open('log.txt', 'a') do |f|
  f.write("%s\t%s\t%s\n" % [Time.now.strftime("%Y-%m-%d_%H:%M:%S"), session_id, redirect_url])
end
`aws --profile #{aws_profile_name} s3 cp log.txt s3://100mentors-redirector/log.txt`
